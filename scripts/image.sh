rm -rf image
jlink --module-path jars/:$JAVA_HOME/jmods \
      --add-modules service.client \
      --add-modules service.provider \
      --add-modules service.provider.b \
      --add-modules service.provider.c \
      --add-modules service.provider.d  \
      --strip-debug \
      --compress=2 \
      --output image
