# $1: module/folder name
# $2: fully qualified path for java main class
echo "$(date) : Running module $1/$2"
java --module-path modules -m $1/$2
