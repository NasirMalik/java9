rm -rf jars && mkdir -p jars
jar --create --file jars/service.api.jar -C modules/service.api .
jar --create --file jars/service.provider.jar -C modules/service.provider .
jar --create --file jars/service.provider.b.jar -C modules/service.provider.b .
jar --create --file jars/service.provider.c.jar -C modules/service.provider.c .
jar --create --file jars/service.provider.d.jar -C modules/service.provider.d .
jar --create --file jars/service.client.jar --main-class=com.nasir.test.Main -C modules/service.client .
