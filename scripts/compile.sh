# $1: is the module/folder name
cd /Users/nasir/personal/Java/Java\ 9+/Java9/
rm -rf modules/$1
echo "$(date) : Removed module/s $1"
echo "$(date) : Compiling src $1 java files into modules/$1"
# javac -d modules/$1 $(find src/$1 -name *.java)
javac --module-source-path src/$1 -d modules $(find src/$1 -name *.java)
