echo "$(date) : Setting up src $1 and module-info"
mkdir -p src/$1/com/nasir/test
echo "module $1 { }"  > src/$1/module-info.java
echo "package com.nasir.test;

public class Main {
    public static void main(String[] args) {
        System.out.println(\" In main method! \");
    }
} "  > src/$1/com/nasir/test/Main.java
