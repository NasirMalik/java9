package com.nasir.test;

import java.util.ServiceLoader;

import com.nasir.api.TestService;

public class Main {
    public static void main(String[] args) {
        for (TestService service : ServiceLoader.load(TestService.class)) {
            service.method1();
        }
    }
}
