module service.provider.c {
    requires service.api;

    provides com.nasir.api.TestService with com.nasir.api.c.TestServiceImplC;
}
