module service.provider.d {
    requires service.api;

    provides com.nasir.api.TestService with com.nasir.api.d.TestServiceImplD;
}
