 package com.nasir.test.httpclient;

import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpResponse;

import java.net.URI;
import java.util.concurrent.CompletableFuture;

import com.nasir.test.httpclient.internal.HttpTesterImpl;

public class HttpTester {

    public String sendResponse(String url) throws Exception {
        return new HttpTesterImpl().sendResponse(url);
    }

}
