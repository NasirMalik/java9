package com.nasir.test.httpclient.internal;

import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpResponse;

import java.net.URI;
import java.util.concurrent.CompletableFuture;

public class HttpTesterImpl {

    public String sendResponse(String url) throws Exception {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .GET()   // this is the default
                .build();

        HttpClient httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)  // this is the default
                .build();

        CompletableFuture<Object> future = httpClient
                .sendAsync(request, HttpResponse.BodyHandler.asString())
                .thenApply(HttpResponse::body);

        return future.join().toString();

    }

}
