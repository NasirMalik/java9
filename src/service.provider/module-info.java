module service.provider {
    requires service.api;

    provides  com.nasir.api.TestService
        with  com.nasir.api.a.TestServiceImpl;
}
