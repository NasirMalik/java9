module service.provider.b {
    requires service.api;

    provides com.nasir.api.TestService with com.nasir.api.b.TestServiceImplB;
}
